reg add "HKCU\Software\Microsoft\Windows\CurrentVersion\Internet Settings" /v ProxyEnable /t REG_DWORD /d 1 /f 
reg add "HKCU\Software\Microsoft\Windows\CurrentVersion\Internet Settings" /v ProxyServer /t REG_SZ /d [PROXY IP:PORT] /f 
reg add "HKCU\Software\Microsoft\Windows\CurrentVersion\Internet Settings" /v ProxyOverride /t REG_SZ /d [LIST OF (IP) EXCEPTIONS. REMEMBER TO WRITE "<local>" TO BYPASS THE PROXY ON LOCAL ADRESSES] /f
pause 

